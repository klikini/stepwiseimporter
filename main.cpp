#include <QApplication>
#include <QtWidgets>
#include <QWizard>

#include "deletionpage.h"
#include "destinationpage.h"
#include "sourcepage.h"
#include "transferpage.h"

QString getDrivePath();
QStringList scanDrive(QString root);

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	QWizard wizard;

	QStringList sources;
	QString destination;

	wizard.addPage(new SourcePage(&wizard, &sources));
	wizard.addPage(new DestinationPage(&wizard, &destination));
	wizard.addPage(new TransferPage(&wizard, &sources, &destination));
	wizard.addPage(new DeletionPage(&wizard, &sources, &destination));

	wizard.setWindowTitle("Stepwise Importer");
	wizard.setMinimumSize(640, 480);
	wizard.setWizardStyle(QWizard::ClassicStyle);

	wizard.show();
	return app.exec();
}
