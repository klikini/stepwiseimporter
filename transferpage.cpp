#include "transferpage.h"
#include "ui_transferpage.h"

TransferPage::TransferPage(QWidget* parent, QStringList* sources, QString* destination) :
	QWizardPage(parent),
	ui(new Ui::TransferPage)
{
	this->sources = sources;
	this->destination = destination;

	ui->setupUi(this);
}

void TransferPage::initializePage()
{
	QWizardPage::initializePage();

	this->uncopiedSources = QStringList(*sources);

	wizard()->hide();
	wizard()->button(QWizard::NextButton)->setDisabled(true);
	copyFiles();
}

void TransferPage::copyFiles()
{
	QProgressDialog* dialog = new QProgressDialog();
	dialog->setWindowTitle(tr("Please wait..."));
	dialog->setModal(true);
	dialog->setMaximum(uncopiedSources.count());
	dialog->setFixedSize(400, 100);
	dialog->setCancelButton(nullptr);
	dialog->show();

	// Create target directory (if necessary)
	QDir newParentDir = QDir(*destination);
	if (!newParentDir.exists()) {
		newParentDir.mkpath(".");
	}

	// Copy files
	int i = 0;
	QString now = QString::number(QDateTime::currentDateTime().toSecsSinceEpoch());
	int attempts;
	foreach (QString file, uncopiedSources) {
		attempts = 0;
		QFile* fileRef = new QFile(file);
		QFileInfo* info = new QFileInfo(file);
		QString newPath = QDir::cleanPath(*destination + QDir::separator() + info->fileName());
		dialog->setLabelText(tr("Copying") + " " + info->fileName());

copyFile:
		if (fileRef->copy(newPath) && attempts <= 1) {
			attempts++;
			uncopiedSources.removeOne(file);

			// Relax IO scheduler
			QThread::msleep(500);
		} else {
			// Make another name
			newPath.insert(newPath.lastIndexOf('.'), "_" + now);

			int choice = QMessageBox::warning(this,
											  tr("Warning"),
											  tr("There is already a file named") + " " + info->fileName() + " " +
											  tr("in the destination folder. The new file will be named ") +
											  QFileInfo(newPath).fileName() + "\n\n" +
											  tr("Click OK to continue importing all files or Abort to stop and exit."),
											  QMessageBox::Ok, QMessageBox::Abort);

			if (choice == QMessageBox::Abort) {
				// Stop and exit
				this->close();
				exit(EXIT_SUCCESS);
			} else {
				// Try again with new name
				goto copyFile;
			}
		}

		delete fileRef;
		delete info;

		dialog->setValue(++i);
		qApp->processEvents(QEventLoop::ProcessEventsFlag::ExcludeUserInputEvents);
	}

	dialog->hide();
	delete dialog;

	if (uncopiedSources.count() == 0) {
success:
		wizard()->button(QWizard::NextButton)->setDisabled(false);
		wizard()->next();
		wizard()->show();
	} else {
		// Display failures
		int result = QMessageBox::critical(this,
										   tr("Error"),
										   tr("These files could not be imported:") + "\n\n" + uncopiedSources.join("\n"),
										   QMessageBox::Retry | QMessageBox::Ignore);

		if (result == QMessageBox::Retry) {
			copyFiles();
		} else {
			// Don't allow these files to be deleted in the next step
			foreach (QString file, uncopiedSources)
			{
				sources->removeOne(file);
			}

			goto success;
		}
	}
}

TransferPage::~TransferPage()
{
	delete ui;
}
