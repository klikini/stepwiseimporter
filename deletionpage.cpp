#include "deletionpage.h"
#include "ui_deletionpage.h"

DeletionPage::DeletionPage(QWidget* parent, QStringList* sources, QString* destination) :
	QWizardPage(parent),
	ui(new Ui::DeletionPage)
{
	this->sources = sources;
	this->destination = destination;

	ui->setupUi(this);
	setLayout(ui->gridLayout);

	connect(ui->deleteButton, SIGNAL(clicked()), this, SLOT(confirmDeleteOriginals()));
	connect(ui->browseButton, SIGNAL(clicked()), this, SLOT(openDestination()));
}

DeletionPage::~DeletionPage()
{
	delete ui;
}

void DeletionPage::showEvent(QShowEvent* event)
{
	QWizardPage::showEvent(event);

	wizard()->button(QWizard::BackButton)->setDisabled(true);
	wizard()->button(QWizard::CancelButton)->setDisabled(true);
}

void DeletionPage::confirmDeleteOriginals()
{
	int confirm = QMessageBox::question(this,
										tr("Confirmation"),
										tr("Are you sure you want to delete the original files from the SD card?"
										   "You might want to make sure they all imported correctly first!"));

	if (confirm == QMessageBox::Yes) {
		deleteOriginals();
	}
}

void DeletionPage::deleteOriginals()
{
	QProgressDialog* status = new QProgressDialog(this);
	status->setWindowTitle("Please wait...");
	status->setLabelText("Deleting originals of imported files");
	status->setMaximum(sources->count());
	status->setModal(true);
	status->show();

	int i = 0;
	foreach (QString file, *sources) {
		if (QFile(file).remove()) {
			sources->removeOne(file);
		}

		status->setValue(++i);
		qApp->processEvents(QEventLoop::ProcessEventsFlag::ExcludeUserInputEvents);
	}

	status->close();
	delete status;

	if (sources->count() == 0) {
		// Success
		QMessageBox::information(this,
								 tr("Complete"),
								 tr("All original files were successfully deleted."));

		ui->deleteButton->setDisabled(true);
	} else {
		// Display failures
		int result = QMessageBox::critical(this,
										   tr("Error"),
										   tr("These files could not be deleted:") + "\n\n" + sources->join("\n"),
										   QMessageBox::Retry | QMessageBox::Ignore);

		if (result == QMessageBox::Retry) {
			deleteOriginals();
		}
	}
}

void DeletionPage::openDestination()
{
	QUrl url = QUrl::fromLocalFile(*destination);
	QDesktopServices::openUrl(url);
}
