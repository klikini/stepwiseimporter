#include "sourcepage.h"
#include "ui_sourcepage.h"

SourcePage::SourcePage(QWidget* parent, QStringList* sources) :
	QWizardPage(parent),
	ui(new Ui::SourcePage)
{
	this->selectedMedia = sources;
	ui->setupUi(this);
	this->setLayout(ui->verticalLayout);

	connect(ui->thumbBiggerBtn, SIGNAL(clicked()), this, SLOT(increaseThumbSize()));
	connect(ui->thumbSmallerBtn, SIGNAL(clicked()), this, SLOT(decreaseThumbSize()));
	connect(ui->mediaList, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChanged()));
}

SourcePage::~SourcePage()
{
	delete ui;
}

void SourcePage::initializePage()
{
	QWizardPage::initializePage();

	ui->thumbStatus->setText(QString());

	QString root = getDrivePath();

	QProgressDialog* status = new QProgressDialog();
	status->setWindowTitle(tr("Please wait..."));
	status->setLabelText(tr("Scanning device for media files"));
	status->setModal(true);
	status->setFixedSize(300, 100);
	status->show();

	QStringList media = scanDrive(root);
	status->setMaximum(media.count());
	showThumbs(media, status);
	status->close();
	delete status;
}

void SourcePage::showEvent(QShowEvent* event)
{
	QWizardPage::showEvent(event);
	wizard()->button(QWizard::NextButton)->setDisabled(true);
}

void SourcePage::resizeEvent(QResizeEvent* event)
{
	QWizardPage::resizeEvent(event);
	resizeThumbs();
}

QString SourcePage::getDrivePath()
{
	QList<QStorageInfo> drives = QList<QStorageInfo>();

	foreach (QStorageInfo drive, QStorageInfo::mountedVolumes()) {
		if (drive.isValid() && drive.isReady() && !drive.name().isEmpty()) {
			drives.append(drive);
		}
	}

	if (drives.count() == 1) {
		// One drive found, use it
		return drives.first().rootPath();
	} else if (drives.count() > 1) {
		// Multiple drives found, pick one
		QString root;
		MultiDriveDialog* dialog = new MultiDriveDialog(nullptr, &drives, &root);

		if (dialog->exec() != QDialog::Accepted) {
			close();
			exit(EXIT_SUCCESS);
		}

		delete dialog;
		return root;
	} else {
		// No drives found
		int result = QMessageBox::critical(nullptr,
										   tr("Error"),
										   tr("An SD card, camera, drive, or other device was not detected.\n\n"
											  "Please make sure it is plugged in (and turned on, if needed), then click Retry.\n\n"
											  "If it is still not detected, wait a second and click Retry again.\n"
											  "To close the program instead, click Cancel."),
										   QMessageBox::Retry | QMessageBox::Cancel);

		if (result == QMessageBox::Retry) {
			// Retry
			return getDrivePath();
		} else {
			// Close
			this->close();
			exit(EXIT_SUCCESS);
		}
	}
}

QStringList SourcePage::scanDrive(QString root)
{
	QStringList found = QStringList();

	QDirIterator it(QDir(root).absolutePath(),
					FSIO::mediaExtensions,
					QDir::NoFilter,
					QDirIterator::Subdirectories);

	while (it.hasNext()) {
		found << it.next();
	}

	found.sort();

	return found;
}

void SourcePage::showThumbs(QStringList filepaths, QProgressDialog* status)
{
	QString* file;

	for (int i = 0; i < filepaths.count();) {
		file = &filepaths[i];
		QIcon icon = QIcon(*file);
		QString caption = QFileInfo(*file).fileName();
		QListWidgetItem* item = new QListWidgetItem(icon, caption);
		item->setData(Qt::UserRole, *file);
		item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
		ui->mediaList->addItem(item);

		status->setValue(++i);
		qApp->processEvents();

		if (status->wasCanceled()) {
			this->close();
			exit(EXIT_SUCCESS);
		}
	}

	ui->thumbStatus->setText(QString::number(filepaths.count()) + tr(" file(s) found"));
	updateWizard();
}

void SourcePage::increaseThumbSize()
{
	resizeThumbs(iconSizeDelta);
}

void SourcePage::decreaseThumbSize()
{
	resizeThumbs(-iconSizeDelta);
}

void SourcePage::resizeThumbs(int delta)
{
	if (delta != 0) {
		int size = ui->mediaList->iconSize().width() + delta;
		ui->mediaList->setIconSize(QSize(size, size));
		ui->thumbSmallerBtn->setDisabled(size <= iconSizeDelta);
	}

	ui->mediaList->doItemsLayout();
}

void SourcePage::selectionChanged()
{
	ui->thumbStatus->setText(
				QString::number(ui->mediaList->selectedItems().count())
				+ tr(" selected "));
	updateWizard();
}

void SourcePage::updateWizard()
{
	if (wizard() != nullptr) {
		wizard()->button(QWizard::NextButton)->setDisabled(ui->mediaList->selectedItems().count() == 0);
	}

	selectedMedia->clear();

	foreach (QListWidgetItem* item, ui->mediaList->selectedItems()) {
		selectedMedia->append(item->data(Qt::UserRole).toString());
		qApp->processEvents(QEventLoop::ProcessEventsFlag::ExcludeUserInputEvents);
	}
}

const QStringList FSIO::mediaExtensions = QStringList()
		// Images
		<< "*.bmp" << "*.gif" << "*.jpg" << "*.jpeg" << "*.png"
		<< "*.BMP" << "*.GIF" << "*.JPG" << "*.JPEG" << "*.PNG"
		   // Videos
		<< "*.mov" << "*.mpg" << "*.mp4" << "*.m4v" << "*.mkv" << "*.avi" << "*.vob"
		<< "*.MOV" << "*.MPG" << "*.MP4" << "*.M4V" << "*.MKV" << "*.AVI" << "*.VOB";
