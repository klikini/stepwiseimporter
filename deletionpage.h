#ifndef DELETIONPAGE_H
#define DELETIONPAGE_H

#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QMessageBox>
#include <QProgressDialog>
#include <QUrl>
#include <QWizardPage>

namespace Ui {
	class DeletionPage;
}

class DeletionPage : public QWizardPage
{
	Q_OBJECT

public:
	explicit DeletionPage(QWidget* parent = nullptr, QStringList* = nullptr, QString* = nullptr);
	~DeletionPage();
	void showEvent(QShowEvent*);

private:
	Ui::DeletionPage* ui;
	QStringList* sources;
	QString* destination;
	void deleteOriginals();

private slots:
	void confirmDeleteOriginals();
	void openDestination();
};

/**
 * @brief deleteMedia deletes all of the files represented by the QFile instances listed.
 * @return A list of files that could not be deleted as QFile objects.
 */
QStringList deleteMedia(QStringList);

#endif // DELETIONPAGE_H
